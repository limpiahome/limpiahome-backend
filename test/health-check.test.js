const supertest = require('supertest');
const app = require('../src/app');

describe('health check test', () => {
  beforeAll(() => {
    app.ready();
  });

  test('should return ok', async () => {
    const response = await supertest(app.server)
      .get('/health-check')
      .expect(200);

    expect(response.body).toHaveProperty('status', 'ok');
  });
});
