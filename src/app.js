const config = require('config');
const fastify = require('fastify');

const { environment, logger } = config;

const app = fastify({ logger });

// ecosystem plugins
app.register(require('fastify-chalk'));
app.register(require('fastify-graceful-shutdown'));

// after loading ecosystem plugins
app.after((err) => {
  if (err) {
    app.log.error('ecosystem plugins\t\t[%s]', app.chalk.red('error'));

    throw err;
  }

  app.log.info('ecosystem plugins\t\t[%s]', app.chalk.magenta('ready'));
});

// system plugins
app.register(require('./routes'));

// after loading system plugins
app.after((err) => {
  if (err) {
    app.log.error('system plugins\t\t[%s]', app.chalk.red('error'));

    throw err;
  }

  // graceful shutdown mechanism
  app.gracefulShutdown((signal, done) => {
    app.log.info('signal\t\t\t[%s]', app.chalk.magenta(signal));

    done();
  });

  app.log.info('system plugins\t\t[%s]', app.chalk.magenta('ready'));
  app.log.info('environment\t\t\t[%s]', app.chalk.magenta(environment));
});

module.exports = app;
