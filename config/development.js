module.exports = {
  // logger
  logger: {
    level: 'debug',
    prettyPrint: {
      colorize: true,
    },
  },
};
